package com.gmail.lecard.jm2048.domain;

import java.util.List;
import java.util.Random;

public class RandomNumberFactory implements NumberFactory {

	private final Random random;

	public RandomNumberFactory(Random random) {
		this.random = random;
	}

	@Override
	public int nextNumber() {
		return random.nextInt(10) == 0 ? 4 : 2;
	}

	@Override
	public int nextPosition(List<Integer> list) {
		int index = (list.size() == 1) ? 0 : random.nextInt(list.size());
		return list.get(index);
	}

}
