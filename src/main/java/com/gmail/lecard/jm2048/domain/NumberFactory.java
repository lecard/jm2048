package com.gmail.lecard.jm2048.domain;

import java.util.List;

public interface NumberFactory {

	int nextNumber();

	int nextPosition(List<Integer> availablePositions);

}
