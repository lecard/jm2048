package com.gmail.lecard.jm2048.domain;

public class NumberToCoordinates {

	private final int width;

	public NumberToCoordinates(int width) {
		this.width = width;
	}

	public int getX(int position) {
		return position % width;
	}

	public int getY(int position) {
		return position / width;
	}
}
