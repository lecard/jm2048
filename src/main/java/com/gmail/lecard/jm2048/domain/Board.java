package com.gmail.lecard.jm2048.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {

	private static final int WIDTH = 4;

	private final int[][] tiles;
	private final NumberFactory numberFactory;
	private final NumberToCoordinates numberToCoordinates = new NumberToCoordinates(WIDTH);

	public Board() {
		this(new int[WIDTH][WIDTH], new RandomNumberFactory(new Random()));
	}

	Board(int[][] tiles, NumberFactory numberFactory) {
		this.tiles = tiles;
		this.numberFactory = numberFactory;
	}

	public void setValor(int x, int y, int valor) {
		tiles[y][x] = valor;
	}

	public int getValor(int x, int y) {
		return tiles[y][x];
	}

	public boolean isEmpty(int x, int y) {
		return getValor(x, y) == 0;
	}

	void createNumber() {
		int nextNumber = numberFactory.nextNumber();

		int position = numberFactory.nextPosition(getAvailablePositions());
		int x = numberToCoordinates.getX(position);
		int y = numberToCoordinates.getY(position);

		setValor(x, y, nextNumber);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int y = 0; y < WIDTH; y++) {
			for (int x = 0; x < WIDTH; x++) {
				int v = getValor(x, y);
				if (v == 0) {
					sb.append("[    ]");
				} else {
					sb.append(String.format("[%4d]", v));
				}
			}
			sb.append("\n");
		}

		return sb.substring(0, sb.length() - 1);
	}

	public List<Integer> getAvailablePositions() {
		List<Integer> positions = new ArrayList<Integer>();

		for (int y = 0; y < WIDTH; y++) {
			for (int x = 0; x < WIDTH; x++) {
				if (isEmpty(x, y)) {
					positions.add((y * WIDTH) + x);
				}
			}
		}
		return positions;
	}

	void onlyMoveRight() {

		for (int x = WIDTH - 1; x > 0; x--) {
			for (int y = 0; y < WIDTH; y++) {

				if (getValor(x, y) == 0) {
					for (int x0 = x - 1; x0 >= 0; x0--) {

						if (!isEmpty(x0, y)) {
							trocar(x, y, x0, y);
							break;
						}
					}
				}

				for (int x0 = x - 1; x0 >= 0; x0--) {
					if (!isEmpty(x0, y)) {
						if (getValor(x0, y) == getValor(x, y)) {
							somar(x, y, x0, y);
						}
						break;
					}
				}
			}
		}
	}

	void onlyMoveLeft() {

		for (int x = 0; x < WIDTH - 1; x++) {
			for (int y = 0; y < WIDTH; y++) {

				if (getValor(x, y) == 0) {
					for (int x0 = x + 1; x0 < WIDTH; x0++) {

						if (!isEmpty(x0, y)) {
							trocar(x, y, x0, y);
							break;
						}
					}
				}

				for (int x0 = x + 1; x0 < WIDTH; x0++) {
					if (!isEmpty(x0, y)) {
						if (getValor(x0, y) == getValor(x, y)) {
							somar(x, y, x0, y);
						}
						break;
					}
				}
			}
		}
	}

	public void onlyMoveUp() {

		for (int y = 0; y < WIDTH - 1; y++) {
			for (int x = 0; x < WIDTH; x++) {

				if (getValor(x, y) == 0) {
					for (int y0 = y + 1; y0 < WIDTH; y0++) {

						if (!isEmpty(x, y0)) {
							trocar(x, y, x, y0);
							break;
						}
					}
				}

				for (int y0 = y + 1; y0 < WIDTH; y0++) {
					if (!isEmpty(x, y0)) {
						if (getValor(x, y0) == getValor(x, y)) {
							somar(x, y, x, y0);
						}
						break;
					}
				}
			}
		}
	}

	public void onlyMoveDown() {

		for (int y = WIDTH - 1; y > 0; y--) {
			for (int x = 0; x < WIDTH; x++) {

				if (getValor(x, y) == 0) {
					for (int y0 = y - 1; y0 >= 0; y0--) {

						if (!isEmpty(x, y0)) {
							trocar(x, y, x, y0);
							break;
						}
					}
				}

				for (int y0 = y - 1; y0 >= 0; y0--) {
					if (!isEmpty(x, y0)) {
						if (getValor(x, y0) == getValor(x, y)) {
							somar(x, y, x, y0);
						}
						break;
					}
				}
			}
		}
	}

	private void trocar(int x, int y, int x2, int y2) {
		setValor(x, y, getValor(x2, y2));
		setValor(x2, y2, 0);
	}

	private void somar(int x, int y, int x2, int y2) {
		int novoValor = getValor(x, y) + getValor(x2, y2);
		setValor(x, y, novoValor);
		setValor(x2, y2, 0);
	}

}