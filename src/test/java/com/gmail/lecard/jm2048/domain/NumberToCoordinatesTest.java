package com.gmail.lecard.jm2048.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NumberToCoordinatesTest {

	private NumberToCoordinates numberToCoordinates = new NumberToCoordinates(4);

	@Test
	public void quandoConvertoPosicaoParaCoordenadas() {
		int position = 0;
		helpTest(position++, 0, 0);
		helpTest(position++, 1, 0);
		helpTest(position++, 2, 0);
		helpTest(position++, 3, 0);
		helpTest(position++, 0, 1);
		helpTest(position++, 1, 1);
		helpTest(position++, 2, 1);
		helpTest(position++, 3, 1);
		helpTest(position++, 0, 2);
		helpTest(position++, 1, 2);
		helpTest(position++, 2, 2);
		helpTest(position++, 3, 2);
		helpTest(position++, 0, 3);
		helpTest(position++, 1, 3);
		helpTest(position++, 2, 3);
		helpTest(position++, 3, 3);
	}

	private void helpTest(int position, int x, int y) {
		final String template = "Posição %d %s";
		assertEquals(String.format(template, position, "x"), x, numberToCoordinates.getX(position));
		assertEquals(String.format(template, position, "y"), y, numberToCoordinates.getY(position));
	}
	
}
