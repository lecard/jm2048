package com.gmail.lecard.jm2048.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RandomNumberFactoryTest {

	private RandomNumberFactory numberFactory;

	@Test
	public void quandoCrioNumero4() {
		Random random = mock(Random.class);
		when(random.nextInt(10)).thenReturn(0);

		numberFactory = new RandomNumberFactory(random);

		assertEquals(4, numberFactory.nextNumber());
	}

	@Test
	public void quandoCrioNumero2() {
		Random random = mock(Random.class);
		when(random.nextInt(10)).thenReturn(1, 2, 3, 4, 5, 6, 7, 8, 9);

		numberFactory = new RandomNumberFactory(random);

		assertEquals(2, numberFactory.nextNumber()); // 1
		assertEquals(2, numberFactory.nextNumber()); // 2
		assertEquals(2, numberFactory.nextNumber()); // 3
		assertEquals(2, numberFactory.nextNumber()); // 4
		assertEquals(2, numberFactory.nextNumber()); // 5
		assertEquals(2, numberFactory.nextNumber()); // 6
		assertEquals(2, numberFactory.nextNumber()); // 7
		assertEquals(2, numberFactory.nextNumber()); // 8
		assertEquals(2, numberFactory.nextNumber()); // 9
	}

	@Test
	public void quandoCrioNumeroAleatorio() {
		Random random = mock(Random.class);
		when(random.nextInt(10)).thenReturn(1, 7, 0, 4, 9, 6, 0, 0, 1);

		numberFactory = new RandomNumberFactory(random);

		assertEquals(2, numberFactory.nextNumber()); // 1
		assertEquals(2, numberFactory.nextNumber()); // 7
		assertEquals(4, numberFactory.nextNumber()); // 0
		assertEquals(2, numberFactory.nextNumber()); // 4
		assertEquals(2, numberFactory.nextNumber()); // 9
		assertEquals(2, numberFactory.nextNumber()); // 6
		assertEquals(4, numberFactory.nextNumber()); // 0
		assertEquals(4, numberFactory.nextNumber()); // 0
		assertEquals(2, numberFactory.nextNumber()); // 1
	}

	@Test
	public void quandoSelecionoEspaçosSoTemUmDisponivelRetornoEstaPosicao() {
		Random random = mock(Random.class);
		numberFactory = new RandomNumberFactory(random);
		assertEquals(3, numberFactory.nextPosition(Arrays.asList(3)));
	}

	@Test
	public void quandoSelecionoEspaçosETemQuatroDisponiveisRetornoUmaDestasPosicoes() {
		Random random = mock(Random.class);
		when(random.nextInt(4)).thenReturn(2);
		numberFactory = new RandomNumberFactory(random);
		assertEquals(11, numberFactory.nextPosition(Arrays.asList(3, 15, 11, 2)));
	}

}
