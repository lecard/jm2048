package com.gmail.lecard.jm2048.domain;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BoardTest {

	@Mock
	private NumberFactory numberFactory;
	private Board board;

	@Test
	public void quandoBuscoComoTextoQuadroPreenchido() {
		int[][] dados = new int[][] { //
				{ 2, 4, 8, 16 }, //
				{ 32, 64, 128, 256 }, //
				{ 512, 1024, 2048, 4096 }, //
				{ 8192, 2, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		String expected = "" + //
				"[   2][   4][   8][  16]\n" + //
				"[  32][  64][ 128][ 256]\n" + //
				"[ 512][1024][2048][4096]\n" + //
				"[8192][   2][   4][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoBuscoComoTextoQuadroVazio() {
		int[][] dados = new int[4][4];
		board = new Board(dados, numberFactory);

		String expected = "" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoBuscoPosicoesDisponiveisETodasEstaoLivres() {
		int[][] dados = new int[4][4];
		board = new Board(dados, numberFactory);

		List<Integer> expected = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
		assertEquals(expected, board.getAvailablePositions());
	}

	@Test
	public void quandoBuscoPosicoesDisponiveisEA() {
		int[][] dados = new int[][] { //
				{ 2, 0, 8, 16 }, //
				{ 0, 64, 128, 256 }, //
				{ 512, 0, 2048, 0 }, //
				{ 8192, 2, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		List<Integer> expected = Arrays.asList(1, 4, 9, 11, 15);
		assertEquals(expected, board.getAvailablePositions());
	}

	@Test
	public void quandoCrioDoisNumerosNovos() {
		// Criar um 4 depois um 2
		when(numberFactory.nextNumber()).thenReturn(4, 2, 2);
		// Criar na posição 0 (0, 0) e depois na posição 2 (0, 2)
		when(numberFactory.nextPosition(anyListOf(Integer.class))).thenReturn(0, 2, 15);

		int[][] dados = new int[4][4];
		board = new Board(dados, numberFactory);

		board.createNumber();

		String expected1 = "" + //
				"[   4][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected1, board.toString());

		board.createNumber();

		String expected2 = "" + //
				"[   4][    ][   2][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected2, board.toString());

		board.createNumber();

		String expected3 = "" + //
				"[   4][    ][   2][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][   2]";

		assertEquals(expected3, board.toString());

	}

	@Test
	public void quandoMovoApenasParaDireitaSimples() {
		int[][] dados = new int[][] { //
				{ 0, 0, 4, 4 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 2, 0, 0 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveRight();

		String expected = "" + //
				"[    ][    ][    ][   8]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][   2]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaDireitaQuatroIguais() {
		int[][] dados = new int[][] { //
				{ 4, 4, 4, 4 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveRight();

		String expected = "" + //
				"[    ][    ][   8][   8]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaDireitaNumeroNoMeio() {
		int[][] dados = new int[][] { //
				{ 4, 2, 4, 4 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 2, 0, 2 }, //
				{ 2, 4, 4, 2 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveRight();

		String expected = "" + //
				"[    ][   4][   2][   8]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][   4]\n" + //
				"[    ][   2][   8][   2]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaDireita() {
		int[][] dados = new int[][] { //
				{ 2, 0, 2, 4 }, //
				{ 8, 8, 8, 8 }, //
				{ 0, 16, 0, 2 }, //
				{ 8192, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveRight();

		String expected = "" + //
				"[    ][    ][   4][   4]\n" + //
				"[    ][    ][  16][  16]\n" + //
				"[    ][    ][  16][   2]\n" + //
				"[    ][    ][    ][8192]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaEsquerdaSimples() {
		int[][] dados = new int[][] { //
				{ 0, 0, 4, 4 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 2, 0, 0 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveLeft();

		String expected = "" + //
				"[   8][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[   2][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaEsquerdaQuatroIguais() {
		int[][] dados = new int[][] { //
				{ 4, 4, 4, 4 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveLeft();

		String expected = "" + //
				"[   8][   8][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaEsquerdaNumeroNoMeio() {
		int[][] dados = new int[][] { //
				{ 0, 8, 0, 8 }, //
				{ 0, 4, 2, 4 }, //
				{ 4, 2, 4, 4 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveLeft();

		String expected = "" + //
				"[  16][    ][    ][    ]\n" + //
				"[   4][   2][   4][    ]\n" + //
				"[   4][   2][   8][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaEsquerda() {
		int[][] dados = new int[][] { //
				{ 2, 0, 2, 4 }, //
				{ 8, 8, 8, 8 }, //
				{ 0, 16, 0, 2 }, //
				{ 8192, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveLeft();

		String expected = "" + //
				"[   4][   4][    ][    ]\n" + //
				"[  16][  16][    ][    ]\n" + //
				"[  16][   2][    ][    ]\n" + //
				"[8192][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaCimaSimples() {
		int[][] dados = new int[][] { //
				{ 0, 0, 4, 0 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 2, 0, 0 }, //
				{ 0, 0, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveUp();

		String expected = "" + //
				"[    ][   2][   8][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaCimaQuatroIguais() {
		int[][] dados = new int[][] { //
				{ 0, 4, 0, 0 }, //
				{ 0, 4, 0, 0 }, //
				{ 0, 4, 0, 0 }, //
				{ 0, 4, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveUp();

		String expected = "" + //
				"[    ][   8][    ][    ]\n" + //
				"[    ][   8][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaCimaQuatroIguaisNumeroNoMeio() {
		int[][] dados = new int[][] { //
				{ 0, 4, 4, 0 }, //
				{ 0, 2, 0, 0 }, //
				{ 0, 4, 4, 0 }, //
				{ 0, 4, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveUp();

		String expected = "" + //
				"[    ][   4][   8][    ]\n" + //
				"[    ][   2][   4][    ]\n" + //
				"[    ][   8][    ][    ]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaCima() {
		int[][] dados = new int[][] { //
				{ 2, 0, 2, 4 }, //
				{ 8, 0, 2, 0 }, //
				{ 0, 16, 2, 2 }, //
				{ 8192, 0, 2, 4 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveUp();

		String expected = "" + //
				"[   2][  16][   4][   4]\n" + //
				"[   8][    ][   4][   2]\n" + //
				"[8192][    ][    ][   4]\n" + //
				"[    ][    ][    ][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaBaixoSimples() {
		int[][] dados = new int[][] { //
				{ 0, 0, 4, 0 }, //
				{ 0, 0, 0, 0 }, //
				{ 0, 2, 4, 0 }, //
				{ 0, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveDown();

		String expected = "" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][   2][   8][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaBaixoQuatroIguais() {
		int[][] dados = new int[][] { //
				{ 0, 4, 4, 0 }, //
				{ 0, 0, 4, 0 }, //
				{ 0, 0, 4, 0 }, //
				{ 0, 0, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveDown();

		String expected = "" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][    ][   8][    ]\n" + //
				"[    ][   4][   8][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaBaixoNumeroNoMeio() {
		int[][] dados = new int[][] { //
				{ 0, 4, 4, 0 }, //
				{ 0, 8, 16, 0 }, //
				{ 0, 4, 4, 0 }, //
				{ 0, 0, 4, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveDown();

		String expected = "" + //
				"[    ][    ][    ][    ]\n" + //
				"[    ][   4][   4][    ]\n" + //
				"[    ][   8][  16][    ]\n" + //
				"[    ][   4][   8][    ]";

		assertEquals(expected, board.toString());
	}

	@Test
	public void quandoMovoApenasParaBaixo() {
		int[][] dados = new int[][] { //
				{ 2, 0, 8, 4 }, //
				{ 8, 8, 8, 4 }, //
				{ 0, 16, 8, 0 }, //
				{ 8192, 0, 0, 0 },//
		};
		board = new Board(dados, numberFactory);

		board.onlyMoveDown();

		String expected = "" + //
				"[    ][    ][    ][    ]\n" + //
				"[   2][    ][    ][    ]\n" + //
				"[   8][   8][   8][    ]\n" + //
				"[8192][  16][  16][   8]";

		assertEquals(expected, board.toString());
	}

}
